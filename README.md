Exercices git : approfondir
===========================

# Objectifs
Revenir sur un commit, créer une branche à partir d'un commit

# Etapes

## Etape 1
Une erreur s'est glissée dans le fichier file_to_update.md : fourth au lieu de third

Revenir sur le commit "Second commit" (voir git log)
Créer une branche "201903-new-master" à partir de ce commit
Faire la correction dans la branche (fourth devient third)
Merger dans master
tagguer la version "en-1.0.0"
Supprimer la branche "201903-new-master"

## Etape 2
Créer une branche "201903-french-version
Traduire le fichier file_to_update.md en français
Merger avec master
Tagguer la version en "fr-1.0.0"

Faire un diff des deux versions

